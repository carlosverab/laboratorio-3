package com.example.cv_laboratorio3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRegistrar.setOnClickListener {

            // validar

            if (edtNombre.text.isEmpty()) {
                mensaje("Ingrese nombre de mascota")
                return@setOnClickListener

            }

            if (edtEdad.text.isEmpty()) {
                mensaje("Ingrese edad de mascota")
                return@setOnClickListener

            }

            var tipoMascota =  ""
            var rImagen : Int

            if (rbPerro.isChecked) {
                tipoMascota = getString(R.string.perro)
                rImagen = 1
            } else if (rbGato.isChecked) {
                tipoMascota = getString(R.string.gato)
                rImagen = 2
            } else {
                tipoMascota = getString(R.string.conejo)
                rImagen = 3
            }

            val nombre      = edtNombre.text.toString()
            val edad        = edtEdad.text.toString()

            var vacunas = ""

            if (cbRabia.isChecked) {
                vacunas = getString(R.string.rabia)
            }
            if (cbDistemper.isChecked) {
                vacunas = concatenaVacunas( getString(R.string.distemper), vacunas)
            }
            if (cbTriple.isChecked) {
                vacunas = concatenaVacunas( getString(R.string.triple), vacunas)
            }
            if (cbParvovirus.isChecked) {
                vacunas = concatenaVacunas( getString(R.string.parvovirus), vacunas)
            }
            if (cbHepatitis.isChecked) {
                vacunas = concatenaVacunas( getString(R.string.hepatitis), vacunas)
            }

            val bundle = Bundle()
            bundle.apply {
                putString("key_nombre", nombre)
                putString("key_edad", edad)
                putString("key_tipoMascota", tipoMascota)
                putString("key_vacunas", vacunas)
                putInt("key_imagen", rImagen)
            }

            val intent = Intent(this, DestinoActivity::class.java).apply {
                putExtras(bundle)
            }

            startActivity(intent)

        }
    }

    // funciones

    fun mensaje(mensaje: String) {

        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show()
    }

    fun concatenaVacunas( vacuna: String, vacunaStr: String) : String {
        if (vacunaStr.isEmpty()) {
            return vacuna
        } else {
            return "$vacunaStr, $vacuna"
        }
    }
}