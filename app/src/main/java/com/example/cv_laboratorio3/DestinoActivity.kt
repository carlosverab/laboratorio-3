package com.example.cv_laboratorio3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.datos_registrados.*

class DestinoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.datos_registrados)

        //Obtenemos el bundle
        val bundle : Bundle? = intent.extras

        //let
        bundle?.let { bundleLibriDeNull ->

            val nombre      =  bundleLibriDeNull.getString("key_nombre","Desconocido")
            val edad        = bundleLibriDeNull.getString("key_edad","0")
            val tipoMascota = bundleLibriDeNull.getString("key_tipoMascota","")
            val vacunas     = bundleLibriDeNull.getString("key_vacunas","")
            val rImagen     = bundleLibriDeNull.getInt("key_imagen", 0)

            tvNombreMascota.text    = nombre
            tvEdadMascota.text      = edad
            tvValTipoMascota.text   = tipoMascota
            tvValVacunas.text       = vacunas

            when (rImagen) {
                1 -> imageView.setImageResource(R.drawable.dog)
                2 -> imageView.setImageResource(R.drawable.cat)
                3 -> imageView.setImageResource(R.drawable.conejo)
            }
        }

    }
}